//
//  File.swift
//  
//
//  Created by Daniel Zanchi on 01/02/22.
//
import SwiftUI

// Shared Extension
@available(iOS 14, macOS 11, *)
extension Color {
	public init(h: CGFloat, s: CGFloat, b: CGFloat, alpha: CGFloat) {
		#if canImport(UIKit)
		self.init(UIColor(hue: h / 360, saturation: s / 100, brightness: b / 100, alpha: alpha))
		#elseif canImport(AppKit)
		self.init(NSColor(hue: h / 360, saturation: s / 100, brightness: b / 100, alpha: alpha))
		#endif
	}
	
	public init(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat = 1.0) {
		#if canImport(UIKit)
		self.init(UIColor(red: r / 255, green: g / 255, blue: b / 255, alpha: a))
		#elseif canImport(AppKit)
		self.init(NSColor(red: r / 255, green: g / 255, blue: b / 255, alpha: a))
		#endif
	}
}

// Platform-specific extensions for modifying colors
@available(iOS 14, macOS 11, *)
extension Color {
	public func modified(withAdditionalHue hue: CGFloat, additionalSaturation: CGFloat, additionalBrightness: CGFloat, additionalAlpha: CGFloat) -> Color {
		#if canImport(UIKit)
		var platformColor = UIColor(self)
		// Implement your UIColor modification logic here.
		return Color(platformColor)
		#elseif canImport(AppKit)
		var platformColor = NSColor(self)
		// Implement your NSColor modification logic here.
		return Color(platformColor)
		#endif
	}
}

// UIColor and NSColor converters
#if canImport(UIKit)
import UIKit

@available(iOS 14, *)
extension Color {
	public var uiColor: UIColor {
		UIColor(self)
	}
}

#endif

#if canImport(AppKit) && !targetEnvironment(macCatalyst)
import AppKit

@available(macOS 11.0, *)
extension Color {
	public var nsColor: NSColor {
		NSColor(self)
	}
}
#endif

