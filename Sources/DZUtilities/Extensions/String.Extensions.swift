//
//  File.swift
//  
//
//  Created by Daniel Zanchi on 23/11/21.
//

import Foundation

extension String {
	
	public enum RandomType { case full, onlyLetters, onlyLowercasedLetters, onlyUppercasedLetters }
	
	static public func randomString(length: Int, randomType: RandomType = .full) -> String {
		var letters = ""
		switch randomType {
		case .full:
			letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
		case .onlyLetters:
			letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
		case .onlyLowercasedLetters:
			letters = "abcdefghijklmnopqrstuvwxyz"
		case .onlyUppercasedLetters:
			letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		}
		
		return String((0..<length).map{ _ in letters.randomElement()! })
	}
	
}

extension String {
	public var localized: String {
		let value = NSLocalizedString(self, comment: self)
		if value != self || NSLocale.preferredLanguages.first == "en" {
			return value
		}
		
		//Fallback a inglese
		guard let path = Bundle.main.path(forResource: "en", ofType: "lproj"),
			  let bundle = Bundle(path: path) else { return value }
		return NSLocalizedString(self, bundle: bundle, comment: "")
	}
	
	public func localized(variables: [String]) -> String {
		let localized = NSLocalizedString(self, comment: self)
		let value = String(format: localized, arguments: variables)
		if value != self || NSLocale.preferredLanguages.first == "en" {
			return value
		}
		
		//Fallback a inglese
		guard let path = Bundle.main.path(forResource: "en", ofType: "lproj"),
			  let bundle = Bundle(path: path) else { return value }
		return String(format: NSLocalizedString(self, bundle: bundle, comment: ""), arguments: variables)
	}
}
