//
//  File.swift
//  
//
//  Created by Daniel Zanchi on 03/01/24.
//

#if canImport(AppKit) && !targetEnvironment(macCatalyst)
import AppKit

extension NSColor {
    
    public convenience init(h: Int, s: Int, b: Int, alpha: CGFloat) {
        self.init(
            hue: CGFloat(h) / 360,
            saturation: CGFloat(s) / 100,
            brightness: CGFloat(b) / 100,
            alpha: CGFloat(alpha)
        )
    }
    
    public func modified(withAdditionalHue hue: CGFloat, additionalSaturation: CGFloat, additionalBrightness: CGFloat, additionalAlpha: CGFloat) -> NSColor {
        
        var currentHue: CGFloat = 0.0
        var currentSaturation: CGFloat = 0.0
        var currentBrigthness: CGFloat = 0.0
        var currentAlpha: CGFloat = 0.0
        
        self.getHue(&currentHue, saturation: &currentSaturation, brightness: &currentBrigthness, alpha: &currentAlpha)
        currentHue = currentHue * 360
        currentSaturation = currentSaturation * 100
        currentBrigthness = currentBrigthness * 100
        
        var resultSaturation = currentSaturation + additionalSaturation
        if resultSaturation < 0 { resultSaturation = 0 }
        if resultSaturation > 100 { resultSaturation = 100 }
        
        var resultBrightness = currentBrigthness + additionalBrightness
        if resultBrightness < 0 { resultBrightness = 0 }
        if resultBrightness > 100 { resultBrightness = 100 }
        
        var resultAlpha = (currentAlpha + additionalAlpha) > 1 ? 1 : (currentAlpha + additionalAlpha)
        if resultAlpha < 0 { resultAlpha = 0 }
        if resultAlpha > 1 { resultAlpha = 1 }
        
        var resultHue = currentHue + hue
        if resultHue < 0 { resultHue = 0 }
        if resultHue > 360 { resultHue = 360 }
        
        return NSColor(h: Int(currentHue + hue),
                       s: Int(resultSaturation),
                       b: Int(resultBrightness),
                       alpha: (resultAlpha))
    }
}
#endif
